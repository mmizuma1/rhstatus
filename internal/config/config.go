package config

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/fatih/color"
	"github.com/spf13/viper"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/prarit/rhstatus/internal/logger"
	"golang.org/x/crypto/ssh/terminal"
)

// Get internal lab logger instance
var log = logger.GetInstance()

const defaultgitlabhost = "https://gitlab.com"

// MainConfig represents the loaded config
var MainConfig *viper.Viper

// New prompts the user for the default config values to use with rhstatus, and save
// them to the provided confpath (default: ~/.config/lab.hcl)
func New(confpath string, r io.Reader) error {
	var (
		reader                                   = bufio.NewReader(r)
		gitlabhost, gitlabtoken, gitlabloadToken string
		err                                      error
	)

	// Set GitLab config (host, token, user)

	confpath = path.Join(confpath, "rhstatus.toml")
	if MainConfig.GetString("core.gitlabhost") == "" {
		fmt.Printf("Enter GitLab host (default: %s): ", defaultgitlabhost)
		gitlabhost, err = reader.ReadString('\n')
		gitlabhost = strings.TrimSpace(gitlabhost)
		if err != nil {
			return err
		}
		if gitlabhost == "" {
			gitlabhost = defaultgitlabhost
		}
	} else {
		// Required to correctly write config
		gitlabhost = MainConfig.GetString("core.gitlabhost")
	}

	MainConfig.Set("core.gitlabhost", gitlabhost)

	gitlabtoken, gitlabloadToken, err = readGitLabPassword(*reader)
	if err != nil {
		return err
	}
	if gitlabtoken != "" {
		MainConfig.Set("core.gitlabtoken", gitlabtoken)
	} else if gitlabloadToken != "" {
		MainConfig.Set("core.gitlabloadtoken", gitlabloadToken)
	}

	bugzillatoken, bugzillaloadToken, err := readBugzillaToken(*reader)
	if err != nil {
		return err
	}
	if bugzillatoken != "" {
		MainConfig.Set("core.bugzillatoken", bugzillatoken)
	} else if bugzillaloadToken != "" {
		MainConfig.Set("core.bugzillaloadtoken", bugzillaloadToken)
	}

	// set default palette
	MainConfig.Set("colors.green", "green")
	MainConfig.Set("colors.grey", "grey")
	MainConfig.Set("colors.red", "red")
	MainConfig.Set("colors.white", "white")
	MainConfig.Set("colors.yellow", "yellow")
	MainConfig.Set("colors.BLUE", "BLUE")

	if err := MainConfig.WriteConfigAs(confpath); err != nil {
		return err
	}
	fmt.Printf("\nConfig saved to %s\n", confpath)

	err = MainConfig.ReadInConfig()
	if err != nil {
		log.Fatal(err)
		UserConfigError()
	}
	return nil
}

var readGitLabPassword = func(reader bufio.Reader) (string, string, error) {
	var loadToken string

	tokenURL, err := url.Parse(MainConfig.GetString("core.gitlabhost"))
	if err != nil {
		return "", "", err
	}
	tokenURL.Path = "profile/personal_access_tokens"

	fmt.Printf("Create a token with scope 'api' here: %s\nEnter default GitLab token, or leave blank to provide a command to load the token: ", tokenURL.String())
	byteToken, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", "", err
	}
	if strings.TrimSpace(string(byteToken)) == "" {
		fmt.Printf("\nEnter command to load the token:")
		loadToken, err = reader.ReadString('\n')
		if err != nil {
			return "", "", err
		}
	} else {
		fmt.Printf("\n")
	}

	if strings.TrimSpace(string(byteToken)) == "" && strings.TrimSpace(loadToken) == "" {
		log.Fatal("Error: No token provided.  A token can be created at ", tokenURL.String())
	}
	return strings.TrimSpace(string(byteToken)), strings.TrimSpace(loadToken), nil
}

var readBugzillaToken = func(reader bufio.Reader) (string, string, error) {
	var loadToken string

	fmt.Printf("Create a bugzilla API token here: https://bugzilla.redhat.com/userprefs.cgi?tab=apikey\n")
	fmt.Printf("Enter Bugzilla token, or leave blank to provide a command to provide the token: ")
	byteToken, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", "", err
	}
	if strings.TrimSpace(string(byteToken)) == "" {
		fmt.Printf("\nEnter command to read the token:")
		loadToken, err = reader.ReadString('\n')
		if err != nil {
			return "", "", err
		}
	}

	if strings.TrimSpace(string(byteToken)) == "" && strings.TrimSpace(loadToken) == "" {
		log.Fatal("Error: No bugzilla token provided")
	}
	return strings.TrimSpace(string(byteToken)), strings.TrimSpace(loadToken), nil
}

func GetGitLabUser(host, token string) string {
	user := MainConfig.GetString("core.gitlabuser")
	if user != "" {
		return user
	}

	lab, _ := gitlab.NewClient(token, gitlab.WithHTTPClient(&http.Client{}), gitlab.WithBaseURL(host+"/api/v4"))
	u, _, err := lab.Users.CurrentUser()
	if err != nil {
		log.Infoln(err)
		UserConfigError()
	}

	MainConfig.Set("core.gitlabuser", u.Username)
	MainConfig.WriteConfig()
	MainConfig.Set("core.gitlabemail", u.Email)
	MainConfig.WriteConfig()
	fmt.Printf("Email and username saved as %s and %s\n", u.Email, u.Username)

	return u.Username
}

// GetGitLabToken returns a token string from the config file.
// The token string can be cleartext or returned from a password manager or
// encryption utility.
func GetGitLabToken() string {
	token := MainConfig.GetString("core.gitlabtoken")
	if token == "" && MainConfig.GetString("core.gitlabloadtoken") != "" {
		// args[0] isn't really an arg ;)
		args := strings.Split(MainConfig.GetString("core.gitlabloadtoken"), " ")
		_token, err := exec.Command(args[0], args[1:]...).Output()
		if err != nil {
			log.Infoln(err)
			UserConfigError()
		}
		token = string(_token)
		// tools like pass and a simple bash script add a '\n' to
		// their output which confuses the gitlab WebAPI
		if token[len(token)-1:] == "\n" {
			token = strings.TrimSuffix(token, "\n")
		}
	}
	return token
}

func GetSubscribedLabels() []string {
	// config.GetString will return an empty string and not nil so we have
	// to check the return first
	labels := MainConfig.GetString("core.subscribedlabels")
	if labels != "" {
		return strings.Split(labels, ",")
	}
	return nil
}

func GetEmail() string {
	return MainConfig.GetString("core.gitlabemail")
}

func GetBugzillaToken() string {
	token := MainConfig.GetString("core.bugzillatoken")
	if token == "" && MainConfig.GetString("core.bugzillaloadtoken") != "" {
		// args[0] isn't really an arg ;)
		args := strings.Split(MainConfig.GetString("core.bugzillaloadtoken"), " ")
		_token, err := exec.Command(args[0], args[1:]...).Output()
		if err != nil {
			log.Infoln(err)
			UserConfigError()
		}
		token = string(_token)
		// tools like pass and a simple bash script add a '\n' to
		// their output which confuses the bugzilla WebAPI
		if token[len(token)-1:] == "\n" {
			token = strings.TrimSuffix(token, "\n")
		}
	}
	return token
}

var rhStatusConfigPath string

func GetHolidays() []time.Time {
	// These files are created from
	// https://source.redhat.com/career/rewards-portfolio/benefits/benefits_wiki
	// and are a copy of the dates.  They are in "Friday, January 1, 2021" format.

	file, err := os.Open(rhStatusConfigPath + "/holidays")
	if err != nil {
		var red = color.New(color.FgRed).SprintFunc()
		fmt.Println(red("WARNING: ~/.config/rhstatus/holidays does not exist.  Deadline"))
		fmt.Println(red("dates may be incorrect.  See rhstatus' README.md for"))
		fmt.Println(red("information on configuring the holidays file."))
		return nil
	}
	defer file.Close()

	var holidays []time.Time
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		t, err := time.Parse("Monday, January 2, 2006", scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		holidays = append(holidays, t)
	}
	return holidays
}

// LoadMainConfig loads the main config file and returns a tuple of
//  host, user, token, ca_file, skipVerify
func LoadMainConfig() (string, string, string) {

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	if wd == home || strings.Contains(wd, "/rhstatus") {
		log.Fatalf("rhstatus must be executed in RHEL work directory\n")
	}

	// Try to find XDG_CONFIG_HOME which is declared in XDG base directory
	// specification and use it's location as the config directory
	confpath := os.Getenv("XDG_CONFIG_HOME")
	if confpath == "" {
		confpath = path.Join(home, ".config")
	}
	rhStatusConfigPath = confpath + "/rhstatus"
	if _, err := os.Stat(rhStatusConfigPath); os.IsNotExist(err) {
		os.MkdirAll(rhStatusConfigPath, 0700)
	}

	MainConfig = viper.New()
	MainConfig.SetConfigName("rhstatus")
	MainConfig.SetConfigType("toml")

	// The local path (aka 'dot slash') does not allow for any
	// overrides from the work tree lab.toml
	MainConfig.AddConfigPath(".")
	MainConfig.AddConfigPath(rhStatusConfigPath)

	if _, ok := MainConfig.ReadInConfig().(viper.ConfigFileNotFoundError); ok {
		// Create a new config
		err := New(rhStatusConfigPath, os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	}

	var (
		GitLabuser  string
		GitLabhost  string
		GitLabtoken string
	)

	if !MainConfig.IsSet("core.gitlabhost") {
		GitLabhost = defaultgitlabhost
	} else {
		GitLabhost = MainConfig.GetString("core.gitlabhost")
	}

	if GitLabtoken = GetGitLabToken(); GitLabtoken == "" {
		UserConfigError()
	}

	GitLabuser = GetGitLabUser(GitLabhost, GitLabtoken)

	return GitLabhost, GitLabuser, GitLabtoken
}

func GetGreen() string {
	green := MainConfig.GetString("colors.green")
	if green == "" {
		MainConfig.Set("colors.green", "green")
		MainConfig.WriteConfig()
		green = "green"
	}
	return green
}

func GetGrey() string {
	grey := MainConfig.GetString("colors.grey")
	if grey == "" {
		MainConfig.Set("colors.grey", "grey")
		MainConfig.WriteConfig()
		grey = "grey"
	}
	return grey
}

func GetRed() string {
	red := MainConfig.GetString("colors.red")
	if red == "" {
		MainConfig.Set("colors.red", "red")
		MainConfig.WriteConfig()
		red = "red"
	}
	return red
}

func GetWhite() string {
	white := MainConfig.GetString("colors.white")
	if white == "" {
		MainConfig.Set("colors.white", "white")
		MainConfig.WriteConfig()
		white = "white"
	}
	return white
}

func GetYellow() string {
	yellow := MainConfig.GetString("colors.yellow")
	if yellow == "" {
		MainConfig.Set("colors.yellow", "yellow")
		MainConfig.WriteConfig()
		yellow = "yellow"
	}
	return yellow
}

func GetBLUE() string {
	BLUE := MainConfig.GetString("colors.BLUE")
	if BLUE == "" {
		MainConfig.Set("colors.BLUE", "BLUE")
		MainConfig.WriteConfig()
		BLUE = "BLUE"
	}
	return BLUE
}

// UserConfigError returns a default error message about authentication
func UserConfigError() {
	fmt.Println("Error: User authentication failed.  This is likely due to a misconfigured Personal Access Token.  Verify the token or token_load config settings before attempting to authenticate.")
	os.Exit(1)
}
