package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/jaytaylor/html2text"
	"gitlab.com/prarit/rhstatus/internal/postfix"
	"gitlab.com/prarit/rhstatus/internal/stack"
)

// This function downloads a rule from pkgs.devel.redhat.com, and
// converts the text into a format that can be used in postfix library.
func getPkgsDevelRule(release string) string {

	input := getPackageRule(release)

	// Replace each string in input ( and ) with a "( " and a " )"
	// so that the braces are correctly separated for strings.Split()
	inputStr := strings.Replace(input, "(", "( ", -1)
	inputStr = strings.Replace(inputStr, ")", " )", -1)
	inputStr = strings.Replace(inputStr, "jiraField ", "jiraField", -1)
	s := strings.Split(inputStr, " ")

	// The previous commands result in screwing up the jiraField entries.
	// Cleanup jiraField entries by removing spaces in variable names and
	// spaces for beginning and end brackets.
	for count, i := range s {
		if i == "jiraField(" {
			start := count
			end := count + 2
			for s[end] != ")" {
				end++
			}
			if start-end == 3 {
				s[start] = s[start] + s[start+1] + ")"
				// zero everything
				s[start+1] = ""
				s[end] = ""
			} else {
				for j := start + 1; j < (end - 1); j++ {
					s[start] += s[j] + "_"
					// zero everything
					s[j] = ""
				}
				s[start] += s[end-1] + ")"
				// zero everything
				s[end-1] = ""
				s[end] = ""
			}
		}
	}
	s = removeEmptyStrings(s)
	return strings.Join(s, " ")
}

// This function removes all logical entries in the input string that begin
// with "jira" by marking logical segements in the input string as true.
// For example, (jiraProject == RHELPLAN) is equated to true, and the
// remaining string is evaluated and returned.  ie) the logical input
// string is returned without any "jira" entries.
func removeJiraEntries(input string, debug bool) (string, []string) {
	var (
		stack     stack.Stack
		variables []string
	)

	s := strings.Split(input, " ")

	for count, i := range s {

		// skip empty strings
		if strings.TrimSpace(i) == "" {
			continue
		}

		// expressions from rules.html are always "variable == literal".  For  example,
		// "release == +".  It is never "+ == release".
		// ie) in postfix "variable literal operator" is always true
		if postfix.IsLogicalOperator(i) {
			// operator
			first := stack.Pop()
			second := stack.Pop()
			if debug {
				fmt.Printf("1 %s popping %s and %s\n", i, first, second)
			}
			if strings.Contains(fmt.Sprintf("%v", first), "jira") {
				if debug {
					fmt.Printf("1 %s is true\n", first)
				}
				first = "true"
			}
			if strings.Contains(fmt.Sprintf("%v", second), "jira") {
				if debug {
					fmt.Printf("1 %s is true\n", second)
				}
				first = "true"
			}
			if first == "true" && second == "true" {
				if debug {
					fmt.Printf("2 %s true and true pushing true\n", i)
				}
				stack.Push("true")
			} else if first != "true" && second == "true" {
				if debug {
					fmt.Printf("3 %s pushing first %s\n", i, first)
				}
				stack.Push(first)
			} else if first == "true" && second != "true" {
				if debug {
					fmt.Printf("4 %s pushing second %s\n", i, second)
				}
				stack.Push(second)
			} else {
				newStr := fmt.Sprintf("%v", second) + " " + fmt.Sprintf("%v", first) + " " + i
				if debug {
					fmt.Printf("5 %s pushing %s\n", i, newStr)
				}
				stack.Push(newStr)
			}

		} else if postfix.IsLogicalOperator(s[count+1]) {
			// literal
			if debug {
				fmt.Printf("6 %s pushing string\n", i)
			}
			stack.Push(i)
		} else {
			// variable
			if !strings.Contains(i, "jira") {
				variables = append(variables, i)
			}
			if debug {
				fmt.Printf("7 %s pushing string\n", i)
			}
			stack.Push(i)
		}
	}
	return fmt.Sprintf("%v", stack.Pop()), variables
}

// This function downloads a package rule for a release.  It is called as
// getPackageRules("rhel-8.5.0").  Note there is no '*' at the beginning of
// the string.
func getPackageRule(release string) string {
	resp, err := http.Get("https://pkgs.devel.redhat.com/rules.html")
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	bodyText := strings.Replace(string(body), "\n", "<br>\n", -1)
	html2textOptions := html2text.Options{
		PrettyTables: true,
		OmitLinks:    true,
	}
	bodyText, _ = html2text.FromString(bodyText, html2textOptions)

	scanner := bufio.NewScanner(strings.NewReader(bodyText))

	// An annoying machine unfriendly thing about the rules.html output
	// is that the output consists of several lines of output that have
	// the same prefix (ex, '*rhel-8.5.0'), and the package rule on the
	// next line.  Luckily, it looks like the first entry is the one
	// that the code always needs.
	printNextLine := false
	for scanner.Scan() {
		if printNextLine {
			return scanner.Text()
			break
		}
		if strings.HasPrefix(scanner.Text(), "*"+release) && strings.HasSuffix(scanner.Text(), "*"+release) {
			printNextLine = true
		}
	}

	return ""
}

// This function determines whether or not the logical string 'eval' is valid.
// The string 'eval' is in postfix notation, and has already had it's variables
// converted to literals (for example, "release == +" is already "+ == +".  The
// function returns equivalency logical failures only ie) and & or's are not
// returned, only "==".  The "falseEvals" that are returned can be used to
// return specific errors.
func rulesAreValid(eval string, display string) (truth bool, falseEvals []string) {
	var evalStack stack.Stack
	var displayStack stack.Stack

	// read the string word by word
	e := strings.Split(eval, " ")
	d := strings.Split(display, " ")
	for o, oper := range e {
		if oper == "" {
			continue
		}

		if !postfix.IsLogicalOperator(oper) {
			// if operand, push
			evalStack.Push(oper)
			displayStack.Push(d[o])
		} else {
			// if operator, pop 2 and evaluate, then push
			first := fmt.Sprintf("%v", evalStack.Pop())
			second := fmt.Sprintf("%v", evalStack.Pop())
			displayFirst := fmt.Sprintf("%v", displayStack.Pop())
			displaySecond := fmt.Sprintf("%v", displayStack.Pop())

			truth := false
			switch oper {
			case "==":
				truth = first == second
			case "and":
				fb, _ := strconv.ParseBool(first)
				sb, _ := strconv.ParseBool(second)
				truth = sb && fb
			case "or":
				fb, _ := strconv.ParseBool(first)
				sb, _ := strconv.ParseBool(second)
				truth = sb || fb
			default:
				log.Fatalf("Unknown operator %s\n", oper)
			}
			evalStack.Push(truth)
			displayStr := fmt.Sprintf("%v is \"%v\"", displaySecond, displayFirst)
			displayStack.Push(displayStr)
			if !truth && oper == "==" {
				falseEvals = append(falseEvals, displayStr)
			}
		}
	}

	// if end, pop for answer
	truth, _ = strconv.ParseBool(fmt.Sprintf("%v", evalStack.Pop()))
	return truth, falseEvals
}
